from cv2 import *
from pyvirtualcam import *
from os import listdir
from os.path import isfile, join
import time
import keyboard as k
import random

#videos = [VideoCapture(f) for f in listdir("videos")]
video = VideoCapture(random.choice(os.listdir("videos")))
print(video)

act = VideoCapture(0)
cam = Camera(width = 640, height = 480, fps = 30)
index = 0
fake = True
while True:
    cap = ""
    dcam,rcam = act.read()
    if not fake:
        d,r = dcam, rcam
    else:
        d,r, = video.read()
    if d:
        r = resize(r, (640, 480))
        r = cvtColor(r, COLOR_BGR2RGBA)
        cam.send(r)
        cam.sleep_until_next_frame()
    if k.is_pressed('q'):
        break
    if k.is_pressed('m'):
        print("Swapped")
        fake = not fake
        while k.is_pressed('m'):
            time.sleep(0.01)
        print("untouched")
    else:
        if fake:
            video = VideoCapture(random.choice(os.listdir("videos")))
act.release()
video.release()
destroyAllWindows()
